import { ActivityService } from "../service/ActivityService"
import { TypeService } from "../service/TypeService"
/** checks whether the activity is a survey and if so, if a follow up survey should be pushed
 *
 * @param auth
 * @param activity_id
 * @param obj
 */
export default async function shouldScheduleActivity(auth: any, activity_id: string, obj: any) {
    const activity = await ActivityService.get(auth, activity_id)
    if (activity == null || activity.length == 0) return null
    // TODO: what to do with non-survey activities?
    if (activity[0].spec == "lamp.survey") {
        const tag = await TypeService.get(auth, activity_id, "lamp.dashboard.survey_description")

        const questions = activity[0].settings
        obj.temporal_slices.map(async (slice: any) => {
            // get appropriate followUp combinations
            const index_q = questions.findIndex((e: any) => e.text == slice.item)            

            const index_a = index_q >= 0 && questions[index_q].options != null 
                ? questions[index_q].options.findIndex((e: any) => e == slice.value) : -1
            let follow_up_activity= index_a >= 0 ? tag.questions[index_q].followUps[index_a] : 1 // FIXME: id 1 is a placeholder for no follow up

            if (follow_up_activity != 1) {
                await scheduleActivity(auth, follow_up_activity, obj.participant_id)
            }
        })
    }
}
/** schedules a follow up activity TODO: ONE TIME???
 *
 * @param auth
 * @param activity_id
 * @param participant_id
 */
const scheduleActivity = async (auth: any, activity_id: string, participant_id: string) => {
    const duplicateActivity = await ActivityService.get(auth, activity_id)
    if (duplicateActivity != null) {
        await ActivityService.parent(auth, participant_id, duplicateActivity[0].name ?? "", true).then(async (res) => {
            // if participant does NOT have this survey yet
            if (!res ?? false) await ActivityService.create(auth, participant_id, duplicateActivity[0])
        })
    }
}
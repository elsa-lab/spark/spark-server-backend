const fs = require('fs');

export function generatePassphrase() {
    const lines = fs.readFileSync("./src/utils/english.txt").toString().split("\n");
    return Array.from({length: 2}, () => {
        let i = Math.floor(Math.random() * lines.length);
        return lines.splice(i,1)
    }).join(' ')
}
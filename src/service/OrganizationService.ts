import { Request, Response, Router } from "express"
import { _verify, _verifyOrganizationPermission } from "./Security"
const jsonata = require("../utils/jsonata") // FIXME: REPLACE THIS LATER WHEN THE PACKAGE IS FIXED
import { PubSubAPIListenerQueue } from "../utils/queue/Queue"
import { Repository, ApiResponseHeaders } from "../repository/Bootstrap"
import { OrganizationRole } from "../model/Researcher"

export class OrganizationService {
  public static _name = "Organization"
  public static Router = Router()

  public static async list(auth: any) {
    const OrganizationRepository = new Repository().getOrganizationRepository()
    // Only system admin can view ALL Organizations
    await _verify(auth, [])
    return await OrganizationRepository._select()
  }

  public static async create(auth: any, parent_id: null, organization: any) {
    // Currently only SYSTEM admin can create organization
    const OrganizationRepository = new Repository().getOrganizationRepository()
    const _ = await _verify(auth, [])
    const data = await OrganizationRepository._insert(organization)

    //publishing data for Organization add api with token = Organization.{_id}
    organization.action = "create"
    organization.organization_id = data
    PubSubAPIListenerQueue?.add(
      { topic: `organization`, token: `Organization.${data}`, payload: organization },
      {
        removeOnComplete: true,
        removeOnFail: true,
      }
    )
    return data
  }

  public static async get(auth: any, organization_id: string) {
    // Currently only SYSTEM admin can view organizations
    const OrganizationRepository = new Repository().getOrganizationRepository()
    organization_id = await _verify(auth, [])
    return await OrganizationRepository._select(organization_id)
  }

  public static async getChildren(auth: any, organization_id: string) {
    // TODO: will this be used?
    const OrganizationRepository = new Repository().getOrganizationRepository()
    // Only allow researchers with admin role to access
    const _ = await _verifyOrganizationPermission(auth, [OrganizationRole.Admin], organization_id)
    return await OrganizationRepository._children(organization_id)
  }

  public static async getMembers(auth: any, organization_id: string) {
    const OrganizationRepository = new Repository().getOrganizationRepository()
    // Only allow researchers with admin role to access
    const _ = await _verifyOrganizationPermission(auth, [OrganizationRole.Admin], organization_id)
    return await OrganizationRepository._members(organization_id)
  }

  public static async addMember(auth: any, organization_id: string, body: any) {
    const OrganizationRepository = new Repository().getOrganizationRepository()
    // Only allow researchers with admin role to access
    const _ = await _verifyOrganizationPermission(auth, [OrganizationRole.Admin], organization_id)
    if (!(Object.values(OrganizationRole).includes(body.role))) {
      throw new Error("404.unkown-organization-role")
    }
    return await OrganizationRepository._addMember(organization_id, body.professional_id, body.role)
  }

  public static async set(auth: any, organization_id: string, organization: any | null) {
    const OrganizationRepository = new Repository().getOrganizationRepository()
    // Currently only SYSTEM admin can set/delete organizations
    const _ = await _verify(auth, [])

    if (organization === null) {
      const data = await OrganizationRepository._delete(organization_id)

      // TODO: implement in PubSubAPI (for notifications)
      //publishing data for organization delete api with token = organization.{organization_id}
      PubSubAPIListenerQueue?.add({
        topic: `organization.*`,
        token: `organization.${organization_id}`,
        payload: { action: "delete", organization_id: organization_id },
      })
      PubSubAPIListenerQueue?.add(
        {
          topic: `organization`,
          token: `organization.${organization_id}`,
          payload: { action: "delete", organization_id: organization_id },
        },
        {
          removeOnComplete: true,
          removeOnFail: true,
        }
      )

      return data
    } else {
      const data = await OrganizationRepository._update(organization_id, organization)

      // TODO: implement in PubSubAPI (for notifications)
      //publishing data for organization update api with token = organization.{organization_id}
      organization.action = "update"
      organization.organization_id = organization_id
      PubSubAPIListenerQueue?.add(
        { topic: `organization.*`, token: `organization.${organization_id}`, payload: organization },
        {
          removeOnComplete: true,
          removeOnFail: true,
        }
      )
      PubSubAPIListenerQueue?.add(
        { topic: `organization`, token: `organization.${organization_id}`, payload: organization },
        {
          removeOnComplete: true,
          removeOnFail: true,
        }
      )
      return data
    }
  }
}

OrganizationService.Router.post("/organization", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    res.json({ data: await OrganizationService.create(req.get("Authorization"), null, req.body) })
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
OrganizationService.Router.put("/organization/:id", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    res.json({ data: await OrganizationService.set(req.get("Authorization"), req.params.id, req.body) })
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
OrganizationService.Router.delete("/organization/:id", async (req: Request, res: Response) => {
  // NOTE: Also deletes researchers belonging to the organization
  res.header(ApiResponseHeaders)
  try {
    res.json({ data: await OrganizationService.set(req.get("Authorization"), req.params.id, null) })
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
OrganizationService.Router.get("/organization/:id", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await OrganizationService.get(req.get("Authorization"), req.params.id) }
    output = typeof req.query.transform === "string" ? jsonata(req.query.transform).evaluate(output) : output
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
OrganizationService.Router.get("/organization", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await OrganizationService.list(req.get("Authorization")) }
    output = typeof req.query.transform === "string" ? jsonata(req.query.transform).evaluate(output) : output
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
OrganizationService.Router.get("/organization/:id/organizations", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await OrganizationService.getChildren(req.get("Authorization"), req.params.id) }
    output = typeof req.query.transform === "string" ? jsonata(req.query.transform).evaluate(output) : output
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
OrganizationService.Router.get("/organization/:id/members", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await OrganizationService.getMembers(req.get("Authorization"), req.params.id) }
    output = typeof req.query.transform === "string" ? jsonata(req.query.transform).evaluate(output) : output
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
OrganizationService.Router.post("/organization/:id/add", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await OrganizationService.addMember(req.get("Authorization"), req.params.id, req.body) }
    output = typeof req.query.transform === "string" ? jsonata(req.query.transform).evaluate(output) : output
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
import { OrganizationRole } from "../model";
import { Repository } from "../repository/Bootstrap"

// The AuthSubject type represents an already-validated authorization that can be reused. 
type AuthSubject = { origin: string; access_key: string; secret_key: string; }

// Converts an Authorization header (`Authorization: Basic btoa('user:pass')` to an object.
// If components are missing, throw a missing credentials error (HTTP 401).
// Otherwise, locate the Credential or throw an error if not found/invalid.
export async function _createAuthSubject(authHeader: string | undefined): Promise<AuthSubject> {
  const CredentialRepository = new Repository().getCredentialRepository()  
  if (authHeader === undefined) throw new Error("401.missing-credentials")
  const authStr = authHeader.replace("Basic", "").trim()
  const auth = (authStr.indexOf(":") >= 0 ? authStr : Buffer.from(authStr, "base64").toString()).split(":", 2)
  if (auth.length !== 2 || !auth[1]) throw new Error("401.missing-credentials")
  let origin = await CredentialRepository._find(auth[0], auth[1] || "*" /* FIXME: this forces password match */)   
  return {
    origin: origin,
    access_key: auth[0],
    secret_key: auth[1]
  }
}

// Simple Role-Based-Access-Control (RBAC) to answer: Can (subject) (verb) (object)?
// The (subject) is indicated as the Authorization header of the HTTP call and passed in here.
// The (verb) is indicated in the function that calls _verify (ie. Activity.create).
// The (object) is provided in thhe URL (usually) and passed in here (either string ID or null for "root").
// - Additionally, the "type" array allows restricting hierarchical ownership of subject -> object.
//   Use [] (empty array) to indicate that ONLY root credentials are allowed to (verb).
export async function _verify(
  authSubject: AuthSubject | string | undefined,
  authType: Array<"self" | "sibling" | "parent"> /* 'root' = [] */,
  authObject?: string | null
): Promise<string> {
  const TypeRepository = new Repository().getTypeRepository()

  // If an actual AuthSubject was not provided, create one first.
  if (authSubject === undefined || typeof authSubject === "string")
    authSubject = await _createAuthSubject(authSubject)
    const isRoot = authSubject.origin === null

  // Patch in the special-cased "me" to the actual authenticated credential.
  // Root credentials (origin is null) are not allowed to substitute the "me" value.
  if (authObject === "me" && !isRoot) {
    authObject = authSubject.origin
  } else if (authObject === "me" && isRoot) {
    throw new Error("400.context-substitution-failed")
  }  
  // Check if `authSubject` is root for a root-only authType.
  if (isRoot) {
    let _owner = !!authObject ? await TypeRepository._owner(authObject ?? "") : undefined
    return authObject as any  
  }

  // Check if `authObject` and `authSubject` are the same || authenticated for  resource * 
  if ((!isRoot && authType.includes("self") && (authSubject.origin === authObject))
      || (JSON.stringify(authType) === JSON.stringify(["self", "sibling", "parent"]) && authObject === undefined))
    return authObject as any 
  
  // Optimization.
  if (!isRoot && (authType.includes("parent") || authType.includes("sibling"))) {
    let _owner = await TypeRepository._owner(authObject ?? "")

    // Check if the immediate parent type of `authObject` is found in `authSubject`'s inheritance tree.
    if (authType.includes("sibling") && (_owner === (await TypeRepository._owner(authSubject.origin)))) {
      return authObject as any
    } else {
      // Check if `authSubject` is actually the parent ID of `authObject` matching the same type as `authSubject`.
      // Do the "parent" check before the "sibling" check since it's more likely to be the case, so short circuit here.
      while(_owner !== null) {
        if (_owner === authSubject.origin)
          return authObject as any
        _owner = await TypeRepository._owner(_owner)
      }
    }
  }

  // We've given the authorization enough chances... fail now.
  throw new Error("403.security-context-out-of-scope")
}

// Checks whether authSubject is member of organization and
// verifies it matches the requested role (authType).
// Returns null if AuthSubject is system administrator.
export async function _verifyOrganizationPermission(
  authSubject: AuthSubject,
  authType: Array<OrganizationRole>,
  organization_id?: string | null,
  project_id?: string | null
): Promise<string | null>  {
  if (!!authSubject && typeof authSubject === "string") {
    // If an actual AuthSubject was not provided, create one first.
    authSubject = (await _createAuthSubject(authSubject))
    if (authSubject.origin === null) {
      // This is a system administrator.
      await _verify(authSubject, [])
      return null
    }
  }

  // Verify authSubject.
  await _verify(authSubject, ["self"], "me")

  if (!!project_id) {
    // Infer the organization that this project belongs to.
    const TypeRepository = new Repository().getTypeRepository()  
    organization_id = await TypeRepository._owner(project_id)
  }

  const ResearcherRepository = new Repository().getResearcherRepository()  
  const data = await ResearcherRepository._select(authSubject.origin)
  if (data.length > 0 && authType.includes(data[0].role!) && data[0].organization == organization_id) {
    // Verify this user is a part of the organization with the requested role.
    return data[0].id as string
  }
  
  throw new Error("403.security-context-out-of-scope")
}

export async function _verifyProjectPermission(
  auth: string,
  project_id: string
): Promise<string>  {
  // FIXME: complete this once project roles are implemented
  const ProjectUserRepository = new Repository().getProjectUserRepository();
  const authSubject = await _createAuthSubject(auth)
  let data = (await ProjectUserRepository._select(authSubject.origin, project_id))
  console.log(data)
  if (!!data) {
    // TODO: Check role permission here
    // TODO: Specify roles / permissions first!
    return project_id
  }
  
  throw new Error("403.security-context-out-of-scope")
}
import { Request, Response, Router } from "express"
import { _verify } from "./Security"
const jsonata = require("../utils/jsonata") // FIXME: REPLACE THIS LATER WHEN THE PACKAGE IS FIXED
import { PubSubAPIListenerQueue } from "../utils/queue/Queue"
import { Repository, ApiResponseHeaders } from "../repository/Bootstrap"
// const permissions = require("../utils/projectPermissions")

export class ProjectRoleService {
  public static _name = "ProjectRole"
  public static Router = Router()

  public static async list(auth: any, parent_id: null) {
    const ProjectRoleRepository = new Repository().getProjectRoleRepository()
    // FIXME: auth based on organization role
    // const _ = await _verify(auth, [])
    return await ProjectRoleRepository._list()
  }

  // public static async list_permissions(auth: any) {
  //   // FIXME: auth based on organization role
  //   return permissions.map((x:any) => (x.name))
  // }

  public static async create(auth: any, project_role: any) {
    const ProjectRoleRepository = new Repository().getProjectRoleRepository()
    // FIXME: auth based on organization role
    // const _ = await _verify(auth, [])
    const data = await ProjectRoleRepository._insert(project_role)
    return data
  }

  public static async get(auth: any, id: string) {
    const ProjectRoleRepository = new Repository().getProjectRoleRepository()
    // FIXME: auth based on organization role
    // researcher_id = await _verify(auth, ["self", "parent"], researcher_id)
    return await ProjectRoleRepository._select(id)
  }

  public static async set(auth: any, id: string, project_role: any) {
    const ProjectRoleRepository = new Repository().getProjectRoleRepository()
    // FIXME: auth based on organization role
    if (project_role !== null) {
      return await ProjectRoleRepository._update(id, project_role)
    } 
    const orig = await ProjectRoleRepository._select(id)
    if (!!orig) {
      return await ProjectRoleRepository._delete(id)
    }
  }
}

ProjectRoleService.Router.post("/project_role", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    res.json({ data: await ProjectRoleService.create(req.get("Authorization"), req.body) })
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
// ProjectRoleService.Router.get("/project_role/permissions", async (req: Request, res: Response) => {
//   res.header(ApiResponseHeaders)
//   try {
//     let output = { data: await ProjectRoleService.list_permissions(req.get("Authorization")) }
//     output = typeof req.query.transform === "string" ? jsonata(req.query.transform).evaluate(output) : output
//     res.json(output)
//   } catch (e:any) {
//     if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
//     res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
//   }
// })
ProjectRoleService.Router.get("/project_role/:id", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await ProjectRoleService.get(req.get("Authorization"), req.params.id) }
    output = typeof req.query.transform === "string" ? jsonata(req.query.transform).evaluate(output) : output
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
ProjectRoleService.Router.get("/project_role", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await ProjectRoleService.list(req.get("Authorization"), null) }
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
ProjectRoleService.Router.put("/project_role/:id", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await ProjectRoleService.set(req.get("Authorization"), req.params.id, req.body) }
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
ProjectRoleService.Router.delete("/project_role/:id", async (req: Request, res: Response) => {
  res.header(ApiResponseHeaders)
  try {
    let output = { data: await ProjectRoleService.set(req.get("Authorization"), req.params.id, null) }
    res.json(output)
  } catch (e:any) {
    if (e.message === "401.missing-credentials") res.set("WWW-Authenticate", `Basic realm="LAMP" charset="UTF-8"`)
    res.status(parseInt(e.message.split(".")[0]) || 500).json({ error: e.message })
  }
})
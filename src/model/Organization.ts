import { Identifier } from "./Type"

export class Organization {
    public id?: Identifier
    public parentOrganization?: Identifier
    public name?: string
    public description?: string
}

import { Identifier, Timestamp } from "./Type"

export class Researcher {
  public id?: Identifier
  public name?: string
  public organization? : Identifier
  public email?: string
  public address?: string
  public role?: OrganizationRole
  public studies?: Identifier[] // FIXME: remove this, when ProjectUser is implemented
}

export enum OrganizationRole {
  Admin = "admin",
  Researcher = "researcher",
  Supporter = "supporter"
}

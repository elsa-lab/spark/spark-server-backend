import { Identifier } from "./Type"

export class ProjectUser {
  public id?: Identifier
  public project_id?: Identifier
  public role?: ProjectRole
}

export class ProjectRole {
  public id?: Identifier
  public name?: string
  public permissions?: string[]
}
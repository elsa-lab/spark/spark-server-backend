import { uuid } from "../Bootstrap"
import { Study } from "../../model/Study"
import { StudyInterface } from "../interface/RepositoryInterface"
import { MongoClientDB } from "../Bootstrap"

export class StudyRepository implements StudyInterface {
  public async _select(id: string | null, parent = false): Promise<Study[]> {
    // FIXME: review, gets projects that this professional is a member of
    // if (parent) {
    //   const projectUsers = await MongoClientDB.collection("project_user").find({ _parent: id })
    //     .maxTimeMS(60000)
    //     .toArray()
    //   let projects: any[] = []
    //   for (const projectUser of projectUsers) {
    //     const project = await MongoClientDB.collection("study").findOne({ _id: projectUser.project_id })
    //     projects.push(project)
    //   }
    //   return (projects as any).map((x:any) => ({
    //     id: x._id,
    //     ...x,
    //     _id: undefined,
    //     _parent: undefined,
    //     _deleted: undefined
    //   }))
    const data = await MongoClientDB.collection("study")
      .find(!!id ? (parent ? { _deleted: false, _parent: id } : { _deleted: false, _id: id }) : { _deleted: false })
      .sort({ timestamp: 1 })
      .limit(2_147_483_647)
      .maxTimeMS(60000)
      .toArray()
    return (data as any).map((x: any) => ({
      id: x._id,
      ...x,
      _id: undefined,
      _parent: undefined,
      _deleted: undefined,
      timestamp: undefined,
      __v: undefined,
    }))
  }
  public async _insert(organization_id: string, object: Study): Promise<string> {
    const _id = uuid()
    await MongoClientDB.collection("study").insertOne({
      _id: _id,
      _parent: organization_id,
      timestamp: new Date().getTime(),
      name: object.name ?? "",
      _deleted: false,
      joinable: object.joinable ?? false
    })
    return _id
  }
  public async _update(study_id: string, object: Study): Promise<{}> {
    const orig: any = await MongoClientDB.collection("study").findOne({ _id: study_id })
    await MongoClientDB.collection("study").findOneAndUpdate(
      { _id: orig._id },
      { $set: { name: object.name ?? orig.name } }
    )
    return {}
  }
  public async _delete(study_id: string): Promise<{}> {
    await MongoClientDB.collection("study").updateOne({ _id: study_id }, { $set: { _deleted: true } })
    return {}
  }
  public async _toggle(study_id: string, object: Study): Promise<{}> {
    const orig: any = await MongoClientDB.collection("study").findOne({ _id: study_id })
    await MongoClientDB.collection("study").findOneAndUpdate(
      { _id: orig._id },
      { $set: { joinable: object.joinable ?? !orig.joinable } }
    )
    return {
      joinable: object.joinable ?? !orig.joinable
    }
  }
  public async _joinable(): Promise<string[]> {
    const data = await MongoClientDB.collection("study")
      .find({ _deleted: false, joinable: true })
      .limit(2_147_483_647)
      .maxTimeMS(60000)
      .toArray()
    return data == null ? [] 
    : (data as any).map((x: any) => ({
      id: x._id,
      name: x.name
    }))
  }
}

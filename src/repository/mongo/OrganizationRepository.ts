import { uuid } from "../Bootstrap"
import { Organization } from "../../model/Organization"
import { OrganizationRole } from "../../model/Researcher"
import { OrganizationInterface } from "../interface/RepositoryInterface"
import { MongoClientDB } from "../Bootstrap"

export class OrganizationRepository implements OrganizationInterface {
  public async _select(id?: string): Promise<any> {
    if (!!id) {
      const data = await MongoClientDB.collection("organization").findOne({ _deleted: false, _id: id })
      return {
        id: data._id,
        ...data,
        _id: undefined,
        _parent: undefined,
        _deleted: undefined,
        timestamp: undefined,
      }
    } else {
      const data = await MongoClientDB.collection("organization")
        .find({ _deleted: false })
        .sort({ timestamp: 1 })
        .maxTimeMS(60000)
        .toArray()
      return (data as any).map((x: any) => ({
        id: x._id,
        ...x,
        _id: undefined,
        _parent: undefined,
        _deleted: undefined,
        timestamp: undefined,
      }))
    }
  }
  public async _insert(object: Organization): Promise<string> {
    const _id = uuid()
    // TODO: should check parentOrganization exists?
    await MongoClientDB.collection("organization").insertOne({
      _id: _id,
      name: object.name ?? "",
      timestamp: new Date().getTime(),
      parent_organization: object.parentOrganization ?? undefined,
      _deleted: false,
    })

    return _id
  }
  public async _update(organization_id: string, object: Organization): Promise<{}> {
    const orig: any = await MongoClientDB.collection("organization").findOne({ _id: organization_id })
    await MongoClientDB.collection("organization").findOneAndUpdate(
      { _id: orig._id },
      { $set: { name: object.name ?? orig.name } }
    )

    return {}
  }
  public async _delete(organization_id: string): Promise<{}> {
    // NOTE: Also deletes researchers belonging to the organization
    await MongoClientDB.collection("researcher").updateMany({ organization_id: organization_id }, { $set: { _deleted: true } })
    await MongoClientDB.collection("organization").updateOne({ _id: organization_id }, { $set: { _deleted: true } })
    return {}
  }

  public async _children(organization_id: string,): Promise<{}> {
    const data = await MongoClientDB.collection("organization")
      .find({ _deleted: false, parent_organization: organization_id })
      .sort({ timestamp: 1 })
      .maxTimeMS(60000)
      .toArray()

    return (data as any).map((x: any) => ({
      id: x._id,
      ...x,
      _id: undefined,
      _parent: undefined,
      _deleted: undefined,
      timestamp: undefined
    }))
  }

  public async _members(organization_id: string): Promise<{}> {
    const data = await MongoClientDB.collection("researcher").find({ _deleted: false, organization_id: organization_id }).maxTimeMS(60000).toArray()
    return (data as any).map((x: any) => ({
      id: x._id,
      ...x,
      _id: undefined,
      _parent: undefined,
      _deleted: undefined,
      timestamp: undefined,
      organization_id: undefined,
    }))
  }

  public async _addMember(organization_id: string, professional_id: string, role: OrganizationRole): Promise<{}> {
    await MongoClientDB.collection("researcher").updateOne(
      { _id: professional_id }, {
        $set: { 
          role: role,
          organization_id: organization_id,
        }
      }
    )
    return {}
  }
}

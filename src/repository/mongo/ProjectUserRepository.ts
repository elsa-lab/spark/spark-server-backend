import { uuid } from "../Bootstrap"
import { ProjectUser } from "../../model/ProjectUser"
import { ProjectUserInterface } from "../interface/RepositoryInterface"
import { MongoClientDB } from "../Bootstrap"

export class ProjectUserRepository implements ProjectUserInterface {
  public async _select(professional_id: string, project_id: string): Promise<ProjectUser | null> {
    const data = await MongoClientDB.collection("project_user")
      .findOne({ _parent: professional_id, project_id: project_id })
    
    if (!!data) {
        return {
        id: data._id,
        ...data,
        _id: undefined,
        _parent: undefined,
      }
    } else {
      return null
    }
  }
  public async _insert(professional_id: string, object: ProjectUser): Promise<{}> {
    const _id = uuid()
    // Assign professional to project with role.
    await MongoClientDB.collection("project_user").insertOne({
      _id: _id,
      _parent: professional_id,
      project_id: object.project_id,
      role: object.role
    } as any)
    return {}
  }
 public async _update(professional_id: string, object: ProjectUser): Promise<{}> {
    // Update the role in already assigned project.
    await MongoClientDB.collection("project_user").updateOne(
      { _parent: professional_id, project_id: object.project_id},
      { $set: { role: object.role} }
    )
    return {}
  }
  public async _delete(id: string): Promise<{}> {
    // Remove professional from project.
    await MongoClientDB.collection("project_user").deleteOne({ _id: id })
    return {}
  }
}

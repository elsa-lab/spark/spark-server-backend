import { uuid } from "../Bootstrap"
import { ProjectRole } from "../../model/ProjectUser"
import { ProjectRoleInterface } from "../interface/RepositoryInterface"
import { MongoClientDB } from "../Bootstrap"

export class ProjectRoleRepository implements ProjectRoleInterface {
  public async _select(id: string): Promise<ProjectRole | null> {
    const data = await MongoClientDB.collection("project_role").findOne({ _id: id})
    return {
      id: data._id,
      ...data,
      _id: undefined
    }
  }
  public async _insert(object: ProjectRole): Promise<string> {
    // TODO: should we allow creating new roles?
    const _id = uuid()
    await MongoClientDB.collection("project_role").insertOne({
      _id: _id,
      name: object.name,
      permissions: object.permissions
    } as any)
    return _id
  }
  public async _update(id: string, object: ProjectRole): Promise<{}> {
    // Update role object.
    // TODO: should we allow updating roles?
    const orig: any = await MongoClientDB.collection("project_role").findOne({ _id: id })
    await MongoClientDB.collection("project_role").findOneAndUpdate(
      { _id: orig._id },
      { $set: { 
          name: object.name ?? orig.name, 
          permissions: object.permissions ?? orig.permissions 
        } 
      }
    )
    return {}
  }
  public async _delete(id: string): Promise<{}> {
    await MongoClientDB.collection("project_role").deleteOne({ _id: id })
    return {}
  }
  public async _list(): Promise<ProjectRole[]> {
    const data = await MongoClientDB.collection("project_role").find({  }).toArray();
    return (data as any).map((x: any) => ({
      id: x._id,
      ...x,
      _id: undefined,
    }))
  }
}
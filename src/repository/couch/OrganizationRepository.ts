import { Organization } from "../../model/Organization";
import { OrganizationInterface } from "../interface/RepositoryInterface";

// TODO: Implement
export class OrganizationRepository implements OrganizationInterface { 
  _select(id?: string | undefined): Promise<Organization[]> {
    throw new Error("Method not implemented.");
  }
  _insert(object: {}): Promise<string> {
    throw new Error("Method not implemented.");
  }
  _update(id: string, object: {}): Promise<{}> {
    throw new Error("Method not implemented.");
  }
  _delete(id: string): Promise<{}> {
    throw new Error("Method not implemented.");
  }
  _children(id: string): Promise<{}> {
    throw new Error("Method not implemented.");
  }
  _members(id: string): Promise<{}> {
    throw new Error("Method not implemented.");
  }
  _addMember(id: string, professional_id: string): Promise<{}> {
    throw new Error("Method not implemented.");
  }
}
import { ProjectRole } from "../../model"
import { ProjectRoleInterface } from "../interface/RepositoryInterface"

export class ProjectRoleRepository implements ProjectRoleInterface {
  _select(id: string): Promise<ProjectRole | null> {
    throw new Error("Method not implemented.")
  }
  _insert(object: {}): Promise<string> {
    throw new Error("Method not implemented.")
  }
  _update(id: string, object: {}): Promise<{}> {
    throw new Error("Method not implemented.")
  }
  _delete(id: string): Promise<{}> {
    throw new Error("Method not implemented.")
  }
  _list(): Promise<ProjectRole[]> {
    throw new Error("Method not implemented.")
  }
}
import { ProjectUser } from "../../model"
import { ProjectUserInterface } from "../interface/RepositoryInterface"

export class ProjectUserRepository implements ProjectUserInterface {
  _select(id?: string | undefined): Promise<ProjectUser | null> {
    throw new Error("Method not implemented.")
  }
  _insert(id: string, object: ProjectUser): Promise<string> {
    throw new Error("Method not implemented.")
  }
  _update(id: string, object: ProjectUser): Promise<{}> {
    throw new Error("Method not implemented.")
  }
  _delete(id: string, ): Promise<{}> {
    throw new Error("Method not implemented.")
  }
}